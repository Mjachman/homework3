﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PhoneBook
{
    internal class ToXML : IPersonRepository
    {
        private readonly string _path;
        public ToXML(string path)
        {
            _path = path;
        }

        public void Save(IEnumerable<Person> persons)
        {
            using (var file = File.Create(_path))

            {
                var serializer = new XmlSerializer(typeof(Person[]));
                serializer.Serialize(file,persons.ToArray());
            }
        }

        public Person[] Load()
        {
            using (var file = File.OpenRead(_path))
            {
                var serializer = new XmlSerializer(typeof(Person[]));
                return (Person[]) serializer.Deserialize(file);
            }
           
        }
    }
}
