﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class Program
    {
        static void Main(string[] args)
        {
            var section = (NameValueCollection) ConfigurationManager.GetSection("AddressBookConfig");

            Console.WriteLine($"{section["Provider"]}: {section["ConnectionString"]}");
        }
    }
}