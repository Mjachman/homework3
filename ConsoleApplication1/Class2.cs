﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneBook;

namespace NUnit.PhoneLibrary
{
    
    public class XMLek
    {
        public void PersonFile_SaveLoadIntegration_InEqualsOut()
        {
            var persons = new Person[]
            {
                new Person
                {
                    FirstName = "Mirek",
                    LastName = "Franca",
                    Email = "mfranca@gmail.com",
                    Phone = "321 654 987"
                },
                new Person("Jarek;Ptak;jptak@gmail.com;123 456 789"),
                new Person
                {
                    FirstName = "Janeeefsefefask",
                    LastName = "Bomba",
                    Email = "jbomba@gmail.com",
                    Phone = "111 111 111"
                }
            };
            var toFile = new ToXML(@"C:\Users\student5\Desktop\XML\plik.xml");
            toFile.Save(persons);
            //var loaded = toFile.Load();
            //Console.WriteLine(loaded);
           
        }
    }
}
