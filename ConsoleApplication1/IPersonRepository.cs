using System.Collections.Generic;

namespace PhoneBook
{
    public interface IPersonRepository
    {
        void Save(IEnumerable<Person> persons);
        Person[] Load();
    }
}