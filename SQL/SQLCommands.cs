﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL
{
    public class SQLCommands
    {
        public void Select(string cokolwiek)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ludzie"].ConnectionString;
            string queryString =$"SELECT * FROM people WHERE name LIKE '%{cokolwiek}%' OR surname LIKE '%{cokolwiek}%' OR phone LIKE '%{cokolwiek}%' OR email LIKE '%{cokolwiek}%'";
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                //using (var dataeader = command.ExecuteReader())
                {
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Console.WriteLine($"{dataReader.GetValue(0)} - {dataReader.GetValue(1)} - {dataReader.GetValue(2)} - {dataReader.GetValue(3)} - {dataReader.GetValue(4)}");
                    }
                }
            }
        }
        public void DisplayAll()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ludzie"].ConnectionString;
            string queryString = "SELECT *  FROM people";
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                //using (var dataeader = command.ExecuteReader())
                {
                    var dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        Console.WriteLine($"{dataReader.GetValue(0)} - {dataReader.GetValue(1)} - {dataReader.GetValue(2)} - {dataReader.GetValue(3)} - {dataReader.GetValue(4)}");
                    }
                }
            }
        }
        public void Add(string name, string surname, string phone, string email)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ludzie"].ConnectionString;
            string queryString = $"INSERT INTO people (name,surname,phone,email) VALUES ('{name}','{surname}','{phone}','{email}')";
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                using (var datawriter = command.ExecuteReader())
                {
                    //var dataReader = command.ExecuteReader();
                    //while (dataReader.Read())
                    //{
                    //    Console.WriteLine($"dodano wpis");
                    //}
                }
            }
        }
        public void Delete(int ID)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ludzie"].ConnectionString;
            string queryString = $"DELETE FROM people WHERE id = {ID}";
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();
                using (var datawriter = command.ExecuteReader())
                {
                   
                }
            }
        }
    }
}
