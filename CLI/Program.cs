﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("wybierz sposób zapisu plików:\n1. Baza danych SQL\n2. plik");
            Fasada facade = new Fasada();
            int input = Convert.ToInt32(Console.ReadLine());
            if (input == 1)
            {
               facade.SQLCMD(); 
            }

            if (input == 2)
            {
                facade.XMLCMD();
            }

        }
    }
}
