﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SQL;
using XMLLibrary;

namespace CLI
{
    public class Fasada
    {
        public void SQLCMD()

        {
            Console.WriteLine("Oplcje SQL:1.Szukaj 2.Pokaż wszystko 3.Usuń 4. Dodaj");
            SQLCommands sel = new SQLCommands();
            int opcja = Convert.ToInt32(Console.ReadLine());

            if (opcja == 1)
            {
                Console.WriteLine("Podaj ciąg znaków");
                sel.Select(Console.ReadLine());
                Console.ReadLine();
            }
            else if (opcja == 2)
            {
                Console.WriteLine("Wyświetlanie wszystkich pozycji");
                sel.DisplayAll();
                Console.ReadLine();
            }
            else if (opcja == 3)
            {
                Console.WriteLine("Usuń pozycję podając jej ID");
                sel.Delete(Convert.ToInt32(Console.ReadLine()));
            }
            else if (opcja == 4)
            {                
                
                Console.WriteLine("Dodaj pozycję");
                Console.WriteLine("Podaj imię");
                string a = Console.ReadLine();
                Console.WriteLine("Podaj nazwisko");
                string b = Console.ReadLine();
                Console.WriteLine("Podaj numer telefonu");
                string c = Console.ReadLine();
                Console.WriteLine("Podaj adres e-mail");
                string d = Console.ReadLine();
                sel.Add(a,b,c,d);
            }
            else
            {
                throw new ArgumentException("Nieprawidłowe wejście");
            }
       

        }
        public void XMLCMD()

        {
            Console.WriteLine("Opcje XML:1.Szukaj 2.Pokaż wszystko 3.Usuń 4. Dodaj");
            XMLCommands sel = new XMLCommands();
            int opcja = Convert.ToInt32(Console.ReadLine());

            if (opcja == 1)
            {
                Console.WriteLine("Podaj ciąg znaków");
                sel.Select(Console.ReadLine());
                Console.ReadLine();
            }
            else if (opcja == 2)
            {
                Console.WriteLine("Wyświetlanie wszystkich pozycji");
                sel.DisplayAll();
                Console.ReadLine();
            }
            else if (opcja == 3)
            {
                Console.WriteLine("Usuń pozycję podając jej ID");
                sel.Delete(Console.ReadLine());
            }
            else if (opcja == 4)
            {

                Console.WriteLine("Podaj id");
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Podaj imię");
                string a = Console.ReadLine();
                Console.WriteLine("Podaj nazwisko");
                string b = Console.ReadLine();
                Console.WriteLine("Podaj numer telefonu");
                string c = Console.ReadLine();
                Console.WriteLine("Podaj adres e-mail");
                string d = Console.ReadLine();
                sel.Add(id,a,b,c,d);
            }
            else
            {
                throw new ArgumentException("Nieprawidłowe wejście");
            }


        }
    }
}
