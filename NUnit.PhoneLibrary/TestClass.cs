﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneBook;

namespace NUnit.PhoneLibrary
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public void TestMethod()
        {
            var person1 = new Person
            {
                FirstName = "Mirek",
                LastName = "Franca",
                Phone = "321 654 987"
            };

            var act = person1.ToString();

            Assert.That(act, Is.EqualTo("Mirek;Franca;;321 654 987"));
        }

        [Test]
        public void Constructor_String_PropertiesOk()
        {
            var person1 = new Person("Jarek;Ptak;jptak@gmail.com;123 456 789");

            Assert.That(person1.FirstName, Is.EqualTo("Jarek"));
            Assert.That(person1.LastName, Is.EqualTo("Ptak"));
            Assert.That(person1.Email, Is.EqualTo("jptak@gmail.com"));
            Assert.That(person1.Phone, Is.EqualTo("123 456 789"));
        }

        [Test]
        public void PersonFile_SaveLoadIntegration_InEqualsOut()
        {
            var personsArray = new Person[]
            {
                new Person
                {
                    FirstName = "Mirek",
                    LastName = "Franca",
                    Email = "mfranca@gmail.com",
                    Phone = "321 654 987"
                },
                new Person("Jarek;Ptak;jptak@gmail.com;123 456 789"),
                new Person
                {
                   FirstName = "Janek",
                   LastName = "Bomba",
                   Email = "jbomba@gmail.com",
                   Phone = "111 111 111"
                }
            };

            var personFile = new PersonFile("plik.csv");

            personFile.Save(personsArray);
            var personLoad = personFile.Load();

            // Czy ludzie się znajdują w tablicy
            Assert.That(personLoad, Is.EquivalentTo(personsArray));
            // Czy są pięknie posortowani
            Assert.That(personLoad, Is.Ordered.By(nameof(Person.LastName)));
        }

        [TearDown]
        public void DeleteFile()
        {
            File.Delete("plik.csv");
        }
    }
}
