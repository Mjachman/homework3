﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace XMLLibrary
{
    class PhoneBook

    {
        public void PersonXML(int id, string name, string surname, string phone, string email)
        {
            Person[] persons = new Person[]
                {new Person(id, name, surname, phone, email)};
            IEnumerable<XElement> xml = from person in persons
                    select
                    new XElement("Person", new XElement("id", person.id), new XElement("name", person.name),
                        new XElement("surname", person.surname),
                        new XElement("phone", person.phone), new XElement("email", person.email));
                //read book data from the data source and construct xml elements
            XElement Xml = new XElement("Persons", xml);
            string sciezka = ConfigurationManager.AppSettings["path"];
            Xml.Save(@sciezka);
        }

        class Person
        {
            public int id;
            public string name;
            public string surname;
            public string phone;
            public string email;

            public Person(int id, string name, string surname, string phone, string email)
            {
                this.id = id;
                this.name = name;
                this.surname = surname;
                this.phone = phone;
                this.email = email;
            }
        }
    }
}
