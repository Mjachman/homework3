﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace XMLLibrary
{
    public class XMLCommands
    {
        public void Add(int id,string name, string surname, string phone, string email)
        {
            string sciezka = ConfigurationManager.AppSettings["path"];
            if (!File.Exists(sciezka))
            {
                PhoneBook per = new PhoneBook();
                per.PersonXML(id, name, surname, phone, email);
            }
            else if (File.Exists(sciezka))
            {
                XDocument xmlDoc = XDocument.Load(sciezka);
                xmlDoc.Elements("Persons")
                .First().Add(new XElement("Person", new XElement("id", $"{id}"), new XElement("name", $"{name}"), new XElement("surname", $"{surname}"), new XElement("phone", $"{phone}"), new XElement("email", $"{email}")));
                xmlDoc.Save(sciezka);
            }

        }

        public void DisplayAll()

        {
            string sciezka = ConfigurationManager.AppSettings["path"];
            if (!File.Exists(sciezka))
            {
                throw new ArgumentException("Nie ma takiego pliku");
            }
            else if (File.Exists(sciezka))
            {
                XDocument xmlDoc = XDocument.Load(sciezka);

                var elements = from ele in xmlDoc.Elements("Persons").Elements("Person")
                    where ele != null
                    select ele;

                foreach (var e in elements)
                {
                    Console.WriteLine(
                        $"{e.Element("id").Value} - {e.Element("name").Value} - {e.Element("surname").Value} - {e.Element("phone").Value} - {e.Element("email").Value}");
                }
            }

        }

        public void Select(string cokolwiek)
        {
            string sciezka = ConfigurationManager.AppSettings["path"];
            if (!File.Exists(sciezka))
            {
                throw new ArgumentException("Nie ma takiego pliku");
            }
            else if (File.Exists(sciezka))
            {
                XDocument xmlDoc = XDocument.Load(sciezka);
                var elements = from ele in xmlDoc.Elements("Persons").Elements("Person")


                    where ele.Element("name").Value.ToUpper().Contains(cokolwiek.ToUpper())
                          || ele.Element("surname").Value.ToUpper().Contains(cokolwiek.ToUpper())
                          || ele.Element("phone").Value.ToUpper().Contains(cokolwiek.ToUpper())
                          || ele.Element("email").Value.ToUpper().Contains(cokolwiek.ToUpper())
                    select ele;

                foreach (var e in elements)
                {
                    Console.WriteLine(
                        $"{e.Element("id").Value} - {e.Element("name").Value} - {e.Element("surname").Value} - {e.Element("phone").Value} - {e.Element("email").Value}");

                }
            }
        }

        public void Delete(string id)
        {
            string sciezka = ConfigurationManager.AppSettings["path"];
            if (!File.Exists(sciezka))
            {
                throw new ArgumentException("Nie ma takiego pliku");
            }
            else if (File.Exists(sciezka))
            {
                XDocument xmlDoc = XDocument.Load(sciezka);
                var elementsToDelete = from ele in xmlDoc.Elements("Persons").Elements("Person")
                    where ele.Element("id").Value.Equals(id)
                    select ele;

                foreach (var e in elementsToDelete)
                {
                    e.Remove();

                }

                xmlDoc.Save(sciezka);
            }
        }
        }
    }


